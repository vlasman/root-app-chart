# Root App chart

## Development

Start the containerd host:

```bash
nerdctl run --detach --privileged --publish=8443:8443 --volume=$PWD:/mnt/root-app-chart --workdir=/mnt/root-app-chart ghcr.io/k3d-io/k3d:5.8.3-dind
```

Create a cluster and fetch the kubeconfig:

```bash
nerdctl exec $(nerdctl ps --format='{{.Names}}' | grep k3d | head -n 1) -- k3d cluster create --api-port=0.0.0.0:8443 --k3s-arg=--disable=traefik@server:0
nerdctl exec $(nerdctl ps --format='{{.Names}}' | grep k3d | head -n 1) -- k3d kubeconfig get k3s-default | sed "s|0.0.0.0|127.0.0.1|g" > ~/.kube/config.k3d.yaml
```

### Delete the environment

```bash
rm -f ~/.kube/config.k3d.yaml
nerdctl rm -f $(nerdctl ps --format='{{.Names}}' | grep k3d | head -n 1)
```
